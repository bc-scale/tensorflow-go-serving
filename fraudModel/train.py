from autoencoder import Autoencoder


def main():
    model = Autoencoder(29)
    model.prepare_data("fraudModel/creditcard.csv" , y="Class")
    model(epochs=12,batch_size=64)
    model.export_saved_model("fraudModel/export/fraud/v1")


if __name__ == "__main__":
    main()